<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Posts;
use App\Http\Resources\PostResource;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Posts::paginate(3);

        // Return collection of articles as a resource

        return PostResource::collection($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check if User is logged in
        $user = Auth::guard('api')->user();
        if (!$user) {
            return response()->json(['status' => 'Not Logged In.'], 422);
        }

        $post = new Posts;

        $title = $request->input('title');

        // Add details from form
        $post->title = $title;
        $post->user_id = $user->id;
        $post->slug = str_slug($title, '-');
        $post->content = $request->input('content');
        $post->image = $request->input('image');


        // Store to dabase and send reply
        if ($post->save()) {
            return new PostResource($post);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($post_slug)
    {
        // Find post based on URL slug (not ID)
        $post = Posts::where('slug', $post_slug)->first();

        // If Post is not found, send error response with 404 status
        if (!$post) {
            return response()->json(['status' => 'No query results for model [App\\Posts].'], 404);
        }

        // Returns the single post resource
        return new PostResource($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post_slug)
    {
        // Check if User is logged in
        $user = Auth::guard('api')->user();
        if (!$user) {
            return response()->json(['status' => 'Not Logged In.'], 422);
        }

        // Find post based on URL slug
        $post = Posts::where('slug', $post_slug)->first();

        // Added a var for title so we can use str_slug
        $title = $request->input('title');

        // Set values for the post
        $post->title = $title;
        $post->user_id = $user->id;
        $post->slug = str_slug($title, '-');
        $post->content = $request->input('content');
        $post->image = $request->input('image');

        // If save successful, return response
        if ($post->save()) {
            return new PostResource($post);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_slug)
    {

        // Check if User is logged in
        $user = Auth::guard('api')->user();
        if (!$user) {
            return response()->json(['status' => 'Not Logged In.'], 422);
        }

        // Find post based on URL slug
        $post = Posts::where('slug', $post_slug)->first();

        // Check if the post is found. If not, send error
        if (!$post) {
            return response()->json(['status' => 'Post Not Found']);
        }

        // Delete post
        $post->delete();

        // Send response that post is deleted
        return response()->json(['status' => 'Post Succesfully Deleted']);

    }
}
