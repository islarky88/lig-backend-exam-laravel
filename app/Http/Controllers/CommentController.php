<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Posts;
use App\Comments;
use App\Http\Resources\CommentResource;
use Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($post_slug)
    {
        // Get post ID first based on URL slug of post
        $postID = Posts::where('slug', $post_slug)->first()->id;

        // Get comments based on the post ID
        $comment = Comments::where('creator_id', $postID)->paginate(5);

        // Returns the single article as a resource
        return CommentResource::collection($comment);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_slug)
    {

        // Check if User is logged in
        $user = Auth::guard('api')->user();
        if (!$user) {
            return response()->json(['status' => 'Not Logged In.'], 422);
        }

        // Instantiates a new comment
        $comment = new Comments;

        // Find the ID of the post to bind to the comment
        $post_id = Posts::where('slug', $post_slug)->first()->id;

        // Set values for the comment to be inserted
        // creator_id is currently post id. problem with FE?
        $comment->creator_id = $post_id;
        $comment->creator_type = $user->id;
        $comment->title = '';
        $comment->commentable_id = $user->id;
        $comment->commentable_type = 'App\\User'; //setting this for userid
        $comment->creator_type = '';
        $comment->body = $request->input('body');

        // If save comment and send respoense
        if ($comment->save()) {
            return new CommentResource($comment);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post_slug, $comment_id)
    {
        // Check if User is logged in
        $user = Auth::guard('api')->user();
        if (!$user) {
            return response()->json(['status' => 'Not Logged In.'], 422);
        }

        // Get ID of the post based on URL slug
        $post = Posts::where('slug', $post_slug)->first();

        // Get the Comment to be updated
        $comment = Comments::where('id', $comment_id)->first();

        // Check if Post and Comment is found
        if($comment == null || $post == null){

            // Send error
            return response()->json(['status' => 'Comment not found'], 200);

        // Check if Comment to be edited is bound to the post
        } elseif ($comment->creator_id != $post->id) {

            // Send error
            return response()->json(['status' => 'Comment does not belong to this Post'], 200);

        } elseif ($comment->commentable_id != $user->id) {

             // Send error
             return response()->json(['status' => 'You cannot edit this comment'], 200);

        }

        // Set new data for the comment
        $comment->body = $request->input('body');

        // Save comment and send response
        if ($comment->save()) {
            return new CommentResource($comment);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($post_slug, $comment_id)
    {

        $user = Auth::guard('api')->user();
        if (!$user) {
            return response()->json(['status' => 'Not Logged In.'], 422);
        }

        // Get ID of the post based on URL slug
        $post = Posts::where('slug', $post_slug)->first();

        // Get the Comment to be updated
        $comment = Comments::where('id', $comment_id)->first();

        // Check if Post and Comment is found
        if($comment == null || $post == null){

            // Send error
            return response()->json(['status' => 'Comment not found'], 200);

        // Check if Comment to be edited is bound to the post
        } elseif ($comment->creator_id != $post->id){

            // Send error
            return response()->json(['status' => 'Comment does not belong to this Post'], 200);

        } elseif ($comment->commentable_id != $user->id) {

            // Send error
            return response()->json(['status' => 'You cannot delete this comment'], 200);

        }

           // Save comment and send response
        if ($comment->delete()) {
            return response()->json(['status' => 'Comment Deleted'], 200);
        }

    }
}
