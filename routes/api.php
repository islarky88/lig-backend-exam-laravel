<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Comments
Route::get('posts/{post_slug}/comments', 'CommentController@index');
Route::post('posts/{post_slug}/comments', 'CommentController@store');
Route::patch('posts/{post_slug}/comments/{comment_id}', ['as' => 'request', 'uses' => 'CommentController@update']);
Route::delete('posts/{post_slug}/comments/{comment_id}', ['as' => 'request', 'uses' => 'CommentController@destroy']);

// Posts
Route::get('posts/{post_slug}', 'PostController@show');
Route::get('posts', 'PostController@index');
Route::post('posts', 'PostController@store');
Route::patch('posts/{post_slug}', 'PostController@update');
Route::delete('posts/{post_slug}', 'PostController@destroy');

// Authentication
Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');







